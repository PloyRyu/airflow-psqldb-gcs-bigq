This DAG is for loading data from psql database to store in Google Cloud storage and then upload it to Google Big Query.

1. Input your parameter in DAG file.
2. Once run docker-compose up. input PostgressDB connection,GCP connection and BQ connection in Airflow console.
    Admin -> Connections
3. Run DAG