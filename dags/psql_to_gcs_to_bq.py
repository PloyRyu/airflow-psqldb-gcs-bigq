from airflow import DAG
from airflow.contrib.operators.postgres_to_gcs_operator import PostgresToGoogleCloudStorageOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from airflow.utils.dates import days_ago
from datetime import timedelta


GCS_BUCKET = "your bucket name"
FILENAME = "folder/your file name"
SQL_QUERY = "your psql query"
DATASET_TABLE = "your bq table"
SCHEMA_FIELDS = [{"name": "yyy", "type": "STRING", "mode": "REQUIRED"},
                {"name": "xxx", "type": "INTEGER", "mode": "NULLABLE"}] # your list of column name

default_args = {
    'start_date': days_ago(1),
    'owner': 'your name',
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'schedule_interval': '@once',

}

# Create DAG

dag = DAG(
    'psql_to_gcs_to_bq',
    default_args=default_args,
    description='Pipeline for ETL postgres to GCS to BigQuery',
)

# TODO:Tasks
t1 = PostgresToGoogleCloudStorageOperator(
        default_args=default_args,
        task_id="psql_to_gcs",
        sql=SQL_QUERY,
        bucket=GCS_BUCKET,
        filename=FILENAME,
        export_format='csv',
        field_delimiter=',',  
        dag=dag,
)

# TODO: load to BigQuery
t2 = GoogleCloudStorageToBigQueryOperator(
    task_id='gcs_to_BQ',
    bucket=GCS_BUCKET,
    source_objects=[FILENAME],
    destination_project_dataset_table=DATASET_TABLE,
    source_format='CSV',
    field_delimiter=',',  
    skip_leading_rows = 1,  
    dag=dag,
)

t1 >> t2 